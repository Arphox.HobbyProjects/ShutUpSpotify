﻿using System;
using System.Diagnostics;

namespace ShutUpSpotify;

public static class ProcessMuter
{
    public static void Mute(Process[] processes)
    {
        foreach (Process process in processes)
            Mute(process);
    }

    public static void Unmute(Process[] processes)
    {
        foreach (Process process in processes)
            Unmute(process);
    }

    private static void Mute(Process process)
    {
        if (process is null) throw new ArgumentNullException(nameof(process));
        VolumeMixer.SetApplicationMute(process.Id, true);
    }

    private static void Unmute(Process process)
    {
        if (process is null) throw new ArgumentNullException(nameof(process));
        VolumeMixer.SetApplicationMute(process.Id, false);
    }
}